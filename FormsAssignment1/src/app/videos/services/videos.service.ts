import {HttpClient} from '@angular/common/http';
import {Observable, Subject} from 'rxjs';
import {Video} from '../models/video.model';
import {Injectable, OnInit} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class VideosService implements OnInit {
  onReloadSubject = new Subject<void>();

  constructor(private httpClient: HttpClient) {
  }

  ngOnInit() {
  }

  getVideos(): Observable<Video[]> {
    return this.httpClient.get<Video[]>('http://localhost:3000/videos');
  }

  postVideo(video: Video): Observable<Video> {
    return this.httpClient.post<Video>('http://localhost:3000/videos', video);
  }
}
