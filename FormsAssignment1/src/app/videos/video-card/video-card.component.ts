import {Component, OnDestroy, OnInit} from '@angular/core';
import {VideosService} from '../services/videos.service';
import {Video} from '../models/video.model';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-video-card',
  templateUrl: './video-card.component.html',
  styleUrls: ['./video-card.component.css']
})
export class VideoCardComponent implements OnInit, OnDestroy {
  videos: Video[];

  subscription: Subscription;

  constructor(private videosService: VideosService) {
  }

  ngOnInit() {
    this.getVideo();
    this.subscription = this.videosService.onReloadSubject.subscribe(() => {
      this.getVideo();
    });
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  getVideo() {
    this.videosService.getVideos().subscribe((videos) => this.videos = videos);
  }
}


