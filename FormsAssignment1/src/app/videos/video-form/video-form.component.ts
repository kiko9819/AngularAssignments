import {Component, OnInit, ViewChild} from '@angular/core';
import {VideosService} from '../services/videos.service';
import {Video} from '../models/video.model';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-video-form',
  templateUrl: './video-form.component.html',
  styleUrls: ['./video-form.component.css']
})
export class VideoFormComponent implements OnInit {
  video: Video;
  videos: Observable<Video[]>;

  @ViewChild('title') title;
  @ViewChild('videoUrl') videoUrl;
  @ViewChild('coverImgUrl') coverImgUrl;
  @ViewChild('description') description;

  constructor(private videosService: VideosService) {
  }

  ngOnInit() {
  }

  onSendData(event): void {
    event.preventDefault();
    this.video = new Video(
      this.title.nativeElement.value,
      this.coverImgUrl.nativeElement.value,
      this.videoUrl.nativeElement.value,
      this.description.nativeElement.value
    );
    this.videosService.postVideo(this.video)
      .subscribe(() => {
          this.videosService.onReloadSubject.next();
        }
      );
  }
}
