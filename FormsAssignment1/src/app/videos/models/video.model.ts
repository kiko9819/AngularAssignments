export class Video {
  public title: string;
  public description: string;
  public coverImageUrl: string;
  public videoUrl: string;

  constructor(title: string, description: string, coverImageUrl: string, videoUrl: string) {
    this.title = title;
    this.description = description;
    this.coverImageUrl = coverImageUrl;
    this.videoUrl = videoUrl;
  }
}
