import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {VideoCardComponent} from '../videos/video-card/video-card.component';

const ROUTES: Routes = [
  {path: '', redirectTo: '/videos', pathMatch: 'full'},
  {path: 'videos', component: VideoCardComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(ROUTES)],
  exports: [RouterModule]
})
export class RoutesModule {
}
