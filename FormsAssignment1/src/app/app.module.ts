import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {VideoFormComponent} from './videos/video-form/video-form.component';
import {VideosComponent} from './videos/videos.component';
import {HttpClientModule} from '@angular/common/http';
import {VideoCardComponent} from './videos/video-card/video-card.component';
import {RoutesModule} from './routes/routes.module';

@NgModule({
  declarations: [
    AppComponent,
    VideoFormComponent,
    VideosComponent,
    VideoCardComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RoutesModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
