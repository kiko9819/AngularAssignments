import {Component, OnInit} from '@angular/core';
import {Employee} from '../employee';

@Component({
  selector: 'app-employees-list',
  templateUrl: './employees-list.component.html',
  styleUrls: ['./employees-list.component.css']
})
export class EmployeesListComponent implements OnInit {
  employees: Employee[] = [];

  constructor() {
  }

  ngOnInit() {
  }

  addToTable(employeeData: Employee) {
    this.employees.push(employeeData);
    console.log(this.employees);
  }

  removeEmployee(index) {
    this.employees.splice(index, 1);
  }

  updateEmployee(newEmployee) {
    const updateEmployee = this.employees.find(this.findIndexToUpdate, newEmployee.id);
    const index = this.employees.findIndex((employee) => updateEmployee.id === employee.id);
    this.employees[index] = newEmployee;
  }

  findIndexToUpdate(newEmployee) {
    return newEmployee.id === this;
  }
}
