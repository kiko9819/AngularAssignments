import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

import {Employee} from '../../employee';

@Component({
  selector: 'app-new-employee-modal',
  templateUrl: './new-employee-modal.component.html',
  styleUrls: ['./new-employee-modal.component.css']
})
export class EditEmployeeModalComponent implements OnInit {
  employeeForm: FormGroup;
  idCounter = 0;

  @ViewChild('lgModal') lgModal;

  @Output() employeeAdded: EventEmitter<Employee> = new EventEmitter<Employee>();
  @Output() employeeUpdated: EventEmitter<Employee> = new EventEmitter<Employee>();

  @Input() editEmployee: Employee;
  @Input() buttonText: string;
  @Input() editMode: boolean;

  constructor(private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    this.employeeForm = this.formBuilder.group({
      name: ['', Validators.required],
      position: ['', Validators.required],
      location: ['', Validators.required],
      department: ['', Validators.required],
      email: ['', Validators.compose(
        [Validators.minLength(6), Validators.required]
      )],
      phoneNumber: ['', Validators.compose(
        [Validators.minLength(10), Validators.required]
      )]
    });
  }

  sendEmployeeData(event) {
    event.preventDefault();
    if (this.employeeForm.invalid) {
      return;
    }
    if (this.editMode) {
      this.employeeUpdated.emit(this.getNewEmployee());
    } else {
      this.employeeAdded.emit(this.getNewEmployee());
    }
    this.lgModal.hide();
    this.employeeForm.reset(this.employeeForm.value);
  }

  getNewEmployee(): Employee {
    return new Employee(
      this.idCounter++,
      this.employeeForm.get('name').value,
      this.employeeForm.get('email').value,
      this.employeeForm.get('location').value,
      this.employeeForm.get('phoneNumber').value,
      this.employeeForm.get('department').value,
      this.employeeForm.get('position').value
    );
  }

  showModal() {
    this.lgModal.show();
    if (this.editMode) {
      this.employeeForm.controls.name.setValue(this.editEmployee.name);
      this.employeeForm.controls.email.setValue(this.editEmployee.email);
      this.employeeForm.controls.location.setValue(this.editEmployee.location);
      this.employeeForm.controls.phoneNumber.setValue(this.editEmployee.phoneNumber);
      this.employeeForm.controls.department.setValue(this.editEmployee.department);
      this.employeeForm.controls.position.setValue(this.editEmployee.position);
    }
  }

  hideModal(){
    this.lgModal.hide();
    this.editMode = false;
  }

}
