import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'shorten'
})
export class ShortenPipe implements PipeTransform {

  transform(value: any, nameLength: number): string {
    if (value.length === nameLength) {
      return value;
    }
    return value.substr(0, nameLength) + ' ...';
  }

}
