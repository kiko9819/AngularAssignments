import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {HeaderComponent} from './header/header.component';
import {TopPlayersComponent} from './players/top-players/top-players.component';
import {FilterComponent} from './players/top-players/filter/filter.component';
import {PlayersComponent} from './players/players.component';
import {AppRoutesModule} from './app-routes/app-routes.module';
import {PlayersListComponent} from './players/players-list/players-list.component';
import {AllPlayersComponent} from './players/all-players/all-players.component';
import {PlayerComponent} from './players/player/player.component';
import {HttpClientModule} from '@angular/common/http';
import {ShortenPipe} from './shorten.pipe';
import {FormsModule} from '@angular/forms';
import {FilterPipe} from './filter.pipe';
import { NotFoundComponent } from './not-found/not-found.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    TopPlayersComponent,
    PlayersComponent,
    FilterComponent,
    PlayersListComponent,
    AllPlayersComponent,
    PlayerComponent,
    ShortenPipe,
    FilterPipe,
    NotFoundComponent
  ],
  imports: [
    BrowserModule,
    AppRoutesModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
