import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {Player} from '../models/player.model';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PlayersService {
  onSortSelect = new Subject<string>();

  constructor(private httpClient: HttpClient) {
  }

  getPlayers(): Observable<Player[]> {
    return this.httpClient.get<Player[]>('http://localhost:3000/players');
  }

  getPlayer(id: number): Observable<Player> {
    return this.httpClient.get<Player>('http://localhost:3000/players/' + id);
  }
}
