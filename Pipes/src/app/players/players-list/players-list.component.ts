import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {PlayersService} from '../services/players.service';
import {Player} from '../models/player.model';
import {Observable, Subscription} from 'rxjs';

@Component({
  selector: 'app-players-list',
  templateUrl: './players-list.component.html',
  styleUrls: ['./players-list.component.css']
})
export class PlayersListComponent implements OnInit, OnDestroy {
  segments;
  sortOption;
  players: Player[];
  playersSubscription: Subscription;

  constructor(private router: Router,
              private route: ActivatedRoute,
              private playersService: PlayersService) {
  }

  ngOnInit(): void {
    this.playersSubscription = this.playersService.onSortSelect.subscribe(
      (sortOption: string) => {
        this.sortOption = sortOption;
      }
    );
    this.playersService.getPlayers().subscribe(
      (players: Player[]) => {
        this.players = players;
      }
    );
    this.route.url.forEach(segment => this.segments = segment.join(''));
  }

  ngOnDestroy(): void {
    this.playersSubscription.unsubscribe();
  }

}
