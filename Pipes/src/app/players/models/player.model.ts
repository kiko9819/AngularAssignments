export class Player {
  public id: number;
  public username: string;
  public name: string;
  public gamesWon: number;
  public gamesLost: number;
  public activeHours: number;
  public registerDate: Date;
  public lastLogin: Date;
  public avatarPath: string;

  constructor(id: number,
              username: string,
              name: string,
              gamesWon: number,
              gamesLost: number,
              activeHours: number,
              registerDate: Date,
              lastLogin: Date,
              avatarPath: string) {
    this.id = id;
    this.username = username;
    this.name = name;
    this.gamesWon = gamesWon;
    this.gamesLost = gamesLost;
    this.activeHours = activeHours;
    this.registerDate = registerDate;
    this.lastLogin = lastLogin;
    this.avatarPath = avatarPath;
  }
}
