import {Component, OnInit} from '@angular/core';
import {PlayersService} from '../../services/players.service';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css']
})
export class FilterComponent implements OnInit {
  sortOptions = ['winPercentage', 'activeHours', 'lastLogin'];
  sortOption = 'activeHours';

  constructor(private playersService: PlayersService) {
  }

  ngOnInit() {
  }

  onOptionSelect() {
    this.playersService.onSortSelect.next(this.sortOption);
  }
}
