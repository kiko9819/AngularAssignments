import {Component, OnInit} from '@angular/core';
import {Player} from '../models/player.model';
import {ActivatedRoute, Params} from '@angular/router';
import {PlayersService} from '../services/players.service';

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.css']
})
export class PlayerComponent implements OnInit {
  playerId: number;
  player: Player;

  constructor(private route: ActivatedRoute, private playersService: PlayersService) {
  }

  ngOnInit(): void {
    this.route.params.subscribe(
      (params: Params) => {
        this.playerId = params.id;
        this.getPlayer();
      }
    );
  }

  getPlayer(): void {
    this.playersService.getPlayer(this.playerId)
      .subscribe((player) => {
          this.player = player;
        }
      );
  }

  get winChance(): number {
    const totalGamesPlayed = this.player.gamesWon + this.player.gamesLost;
    return this.player.gamesWon / totalGamesPlayed;
  }
}
