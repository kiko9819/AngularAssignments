import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {TopPlayersComponent} from '../players/top-players/top-players.component';
import {PlayersComponent} from '../players/players.component';
import {AllPlayersComponent} from '../players/all-players/all-players.component';
import {PlayerComponent} from '../players/player/player.component';
import {NotFoundComponent} from '../not-found/not-found.component';

const ROUTES: Routes = [
  {path: '', redirectTo: '/players', pathMatch: 'full'},
  {
    path: 'players', component: PlayersComponent, children: [
      {
        path: 'all-players', component: AllPlayersComponent, children: [
          {path: ':id', component: PlayerComponent},
        ]
      },
      {
        path: 'top-players', component: TopPlayersComponent, children: [
          {path: ':id', component: PlayerComponent},
        ]
      },
    ]
  },
  {path: 'not-found', component: NotFoundComponent},
  {path: '**', redirectTo: '/not-found'}
];

@NgModule({
  imports: [RouterModule.forRoot(ROUTES)],
  exports: [RouterModule]
})
export class AppRoutesModule {
}
