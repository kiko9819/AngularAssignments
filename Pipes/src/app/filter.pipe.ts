import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'filter',
  pure: false
})
export class FilterPipe implements PipeTransform {

  transform(players: any, filterType: string): any {
    let topPlayers = [];

    if (players.length === 0) {
      return players;
    }
    topPlayers = this.filter(players, filterType);

    return topPlayers;
  }

  filter(players, filterType) {
    let resultArr = [];

    if (filterType === 'winPercentage') {
      return players.sort((prevPlayer, nextPlayer) => {
        const prevPlayerWinsPercent = prevPlayer['gamesWon'] / (prevPlayer['gamesWon'] + prevPlayer['gamesLost']);
        const nextPlayerWinsPercent = nextPlayer['gamesWon'] / (nextPlayer['gamesWon'] + nextPlayer['gamesLost']);

        if (prevPlayerWinsPercent < nextPlayerWinsPercent) {
          return -1;
        } else if (prevPlayerWinsPercent > nextPlayerWinsPercent) {
          return 1;
        }
        return 0;
      });
    } else {
      resultArr = players.sort((prevPlayer, nextPlayer) => {
        if (prevPlayer[filterType] < nextPlayer[filterType]) {
          return -1;
        } else if (prevPlayer[filterType] > nextPlayer[filterType]) {
          return 1;
        }
        return 0;
      });
    }
    return resultArr;
  }
}
