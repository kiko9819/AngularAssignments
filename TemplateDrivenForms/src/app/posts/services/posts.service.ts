import {Injectable} from '@angular/core';
import {Post} from '../models/post.model';
import {Observable, Subject} from 'rxjs';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PostsService {
  onCreatePost = new Subject();

  constructor(private httpClient: HttpClient) {
  }

  createPost(post: Post): Observable<Post> {
    return this.httpClient.post<Post>('http://localhost:3000/posts', post);
  }

  getPosts(): Observable<Post[]> {
    return this.httpClient.get<Post[]>('http://localhost:3000/posts');
  }
}
