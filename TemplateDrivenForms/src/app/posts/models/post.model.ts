export class Post {
  public title: string;
  public content: string;
  public imgPath: string;
  public videoPath: string;

  constructor(title: string, content: string, imgPath: string, videoPath: string) {
    this.title = title;
    this.content = content;
    this.imgPath = imgPath;
    this.videoPath = videoPath;
  }
}
