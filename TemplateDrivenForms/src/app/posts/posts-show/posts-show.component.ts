import {Component, OnDestroy, OnInit} from '@angular/core';
import {Post} from '../models/post.model';
import {PostsService} from '../services/posts.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-posts-show',
  templateUrl: './posts-show.component.html',
  styleUrls: ['./posts-show.component.css']
})
export class PostsShowComponent implements OnInit, OnDestroy {
  posts: Post[];

  subscription: Subscription;

  constructor(private postsService: PostsService) {
  }

  ngOnInit() {
    this.getPosts();
    this.subscription = this.postsService.onCreatePost.subscribe(() => {
      this.getPosts();
    });
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  getPosts(): void {
    this.postsService.getPosts().subscribe(posts => this.posts = posts);
  }

}
