import {Component, OnInit, ViewChild} from '@angular/core';

import {PostsService} from '../services/posts.service';
import {Post} from '../models/post.model';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-posts-form',
  templateUrl: './posts-form.component.html',
  styleUrls: ['./posts-form.component.css']
})
export class PostsFormComponent implements OnInit {
  post: Post;
  submitted = false;
  @ViewChild('postsForm') postsForm: NgForm;

  constructor(private postsService: PostsService) {
  }

  ngOnInit() {
  }

  onSubmit() {
    this.submitted = true;
    this.generateNewPost();
    this.postsService.createPost(this.post).subscribe(() => {
      this.postsService.onCreatePost.next();
    });
  }

  private generateNewPost(): void {
    this.post = new Post(
      this.postsForm.value.title,
      this.postsForm.value.content,
      this.postsForm.value.paths.imgPath,
      this.postsForm.value.paths.videoPath
    );
  }
}
