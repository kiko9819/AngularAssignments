import {Directive} from '@angular/core';
import {AbstractControl, FormControl, NG_VALIDATORS, Validator, ValidatorFn} from '@angular/forms';

function validateUrl(): ValidatorFn {
  return (control: AbstractControl) => {
    if (control.value) {
      console.log(control);
      const isValid = control.value.startsWith('https') && control.value.includes('.com');

      if (isValid) {
        return null;
      }
      return {
        videoPath: {
          valid: false
        }
      };
    }
  };
}

@Directive({
  selector: '[appVideoUrlValidator][ngModel]',
  providers: [
    {provide: NG_VALIDATORS, useExisting: VideoUrlValidatorDirective, multi: true}
  ]
})
export class VideoUrlValidatorDirective implements Validator {
  validator: ValidatorFn;

  constructor() {
    this.validator = validateUrl();
  }

  validate(control: FormControl) {
    return this.validator(control);
  }

}
