import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {PostsShowComponent} from '../posts/posts-show/posts-show.component';

const ROUTES: Routes = [
  {path: '', redirectTo: '/posts', pathMatch: 'full'},
  {path: 'posts', component: PostsShowComponent}
];

@NgModule({
  imports: [
    RouterModule.forRoot(ROUTES)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutesModule {
}
