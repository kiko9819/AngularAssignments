import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { PostsComponent } from './posts/posts.component';
import { PostsFormComponent } from './posts/posts-form/posts-form.component';
import { PostsShowComponent } from './posts/posts-show/posts-show.component';
import {AppRoutesModule} from './routes/routes.module';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import { VideoUrlValidatorDirective } from './posts/directives/video-url-validator.directive';

@NgModule({
  declarations: [
    AppComponent,
    PostsComponent,
    PostsFormComponent,
    PostsShowComponent,
    VideoUrlValidatorDirective
  ],
  imports: [
    BrowserModule,
    AppRoutesModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
