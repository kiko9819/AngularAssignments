import {Component, OnInit} from '@angular/core';
import {ModalNotifyService} from '../modal-notify.service';

@Component({
  selector: 'app-login-modal',
  templateUrl: './login-modal.component.html',
  styleUrls: ['./login-modal.component.css']
})
export class LoginModalComponent implements OnInit {

  constructor(private loginNotificationService: ModalNotifyService) {
  }

  ngOnInit() {
  }

  onSubmit(): void {
    this.loginNotificationService.onLoginConfirmed.next();
  }

  onHide(): void {
    this.loginNotificationService.onLoginCanceled.next();
  }


}
