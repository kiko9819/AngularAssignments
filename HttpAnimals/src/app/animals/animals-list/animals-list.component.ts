import {Component, OnInit} from '@angular/core';
import {AnimalsService} from '../services/animals.service';
import {Animal} from '../models/animal.model';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-animals-list',
  templateUrl: './animals-list.component.html',
  styleUrls: ['./animals-list.component.css']
})
export class AnimalsListComponent implements OnInit {
  animals: Animal[];
  animalsFetchSubscription: Subscription;

  constructor(private animalsService: AnimalsService) {
  }

  ngOnInit() {
    this.getAnimals();
    this.animalsFetchSubscription = this.animalsService.onFetch.subscribe(() => {
      this.getAnimals();
    });
  }

  private getAnimals(): void {
    this.animalsService.fetchAnimals().subscribe((animals) => {
      this.animals = animals;
    });
  }

}
