import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {Animal} from '../models/animal.model';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AnimalsService {
  onFetch = new Subject<void>();
  endpoint = 'http://localhost:3000/animals';

  constructor(private httpClient: HttpClient) {
  }

  fetchAnimals(): Observable<Animal[]> {
    return this.httpClient.get<Animal[]>(this.endpoint);
  }

  saveAnimals(animal: Animal): Observable<Animal> {
    return this.httpClient.post<Animal>(this.endpoint, animal);
  }
}
