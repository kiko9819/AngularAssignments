export class Animal {
  id: number;
  name: string;
  type: string;
  age: number;
  imagePath: string;

  constructor(id: number, name: string, type: string, age: number, imagePath: string) {
    this.id = id;
    this.name = name;
    this.type = type;
    this.age = age;
    this.imagePath = imagePath;
  }
}
