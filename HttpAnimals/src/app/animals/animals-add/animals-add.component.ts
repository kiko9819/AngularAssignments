import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Subscription} from 'rxjs';
import {AnimalsService} from '../services/animals.service';

@Component({
  selector: 'app-animals-add',
  templateUrl: './animals-add.component.html',
  styleUrls: ['./animals-add.component.css']
})
export class AnimalsAddComponent implements OnInit, OnDestroy {
  animalsFormGroup: FormGroup;
  animalId = 0;

  animalsSubscription: Subscription;

  constructor(private formBuilder: FormBuilder, private animalsService: AnimalsService) {
  }

  ngOnInit(): void {
    this.buildAnimalsForm();
  }

  ngOnDestroy(): void {
    if (this.animalsSubscription) {
      this.animalsSubscription.unsubscribe();
    }
  }

  onSubmit(): void {
    const animalData = this.animalsFormGroup.value;
    this.animalsSubscription = this.animalsService.saveAnimals(animalData).subscribe(() => {
      alert('Saved an animal.');
    });
    this.animalsFormGroup.reset();
  }

  private buildAnimalsForm(): void {
    this.animalsFormGroup = this.formBuilder.group({
      id: this.animalId++,
      name: ['', Validators.required],
      type: ['', Validators.required],
      age: ['', [Validators.required, Validators.min(0)]],
      imagePath: ['', Validators.required]
    });
  }

}
