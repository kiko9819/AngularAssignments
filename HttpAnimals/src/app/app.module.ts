import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';

import {AppComponent} from './app.component';
import {AnimalsComponent} from './animals/animals.component';
import {AnimalsListComponent} from './animals/animals-list/animals-list.component';
import {AnimalsAddComponent} from './animals/animals-add/animals-add.component';
import {HeaderComponent} from './header/header.component';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';
import {AppRoutesModule} from './app-routes/app-routes.module';

import {AccordionModule, ModalModule} from 'ngx-bootstrap';
import {HomeComponent} from './home/home.component';
import {AuthGuard} from './auth/auth-guard.service';
import {AuthService} from './auth/auth.service';
import {LoginModalComponent} from './login-modal/login-modal.component';
import {LogoutModalComponent} from './logout-modal/logout-modal.component';

@NgModule({
  declarations: [
    AppComponent,
    AnimalsComponent,
    AnimalsListComponent,
    AnimalsAddComponent,
    HeaderComponent,
    PageNotFoundComponent,
    HomeComponent,
    LoginModalComponent,
    LogoutModalComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutesModule,
    ReactiveFormsModule,
    AccordionModule.forRoot(),
    ModalModule.forRoot()
  ],
  providers: [AuthGuard, AuthService],
  entryComponents: [LoginModalComponent, LogoutModalComponent],
  bootstrap: [AppComponent]
})
export class AppModule {
}
