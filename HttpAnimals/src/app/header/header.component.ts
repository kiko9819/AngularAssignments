import {Component, OnDestroy, OnInit} from '@angular/core';
import {AnimalsService} from '../animals/services/animals.service';
import {AuthService} from '../auth/auth.service';
import {Router} from '@angular/router';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {LoginModalComponent} from '../login-modal/login-modal.component';
import {Subscription} from 'rxjs';
import {ModalNotifyService} from '../modal-notify.service';
import {LogoutModalComponent} from '../logout-modal/logout-modal.component';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, OnDestroy {
  isLoggedIn = false;
  bsModalRef: BsModalRef;

  loginConfirmedSubscription: Subscription;
  loginCanceledSubscription: Subscription;

  logoutConfirmationSubscription: Subscription;
  logoutDeclineSubscription: Subscription;

  constructor(private animalsService: AnimalsService,
              private authService: AuthService,
              private bsModalService: BsModalService,
              private modalNotificationService: ModalNotifyService,
              private router: Router) {
  }

  ngOnInit() {
    this.isLoggedIn = this.authService.isLoggedIn;

    this.loginConfirmedSubscription = this.modalNotificationService.onLoginConfirmed.subscribe(
      () => {
        this.loginUser();
      }
    );
    this.loginCanceledSubscription = this.modalNotificationService.onLoginCanceled.subscribe(
      () => {
        this.hideModal();
      }
    );
    this.logoutConfirmationSubscription = this.modalNotificationService.onLogoutConfirmed.subscribe(
      () => {
        this.logoutUser();
      }
    );
    this.logoutDeclineSubscription = this.modalNotificationService.onLogoutDeclined.subscribe(
      () => {
        this.hideModal();
      }
    );
  }

  ngOnDestroy(): void {
    if (this.loginConfirmedSubscription) {
      this.loginConfirmedSubscription.unsubscribe();
    }
    if (this.loginCanceledSubscription) {
      this.loginCanceledSubscription.unsubscribe();
    }
  }

  onFetch(): void {
    this.animalsService.onFetch.next();
  }

  loginUser(): void {
    this.authService.logIn();
    this.isLoggedIn = this.authService.isLoggedIn;
    this.router.navigate(['/animals']);
    this.hideModal();
  }

  onLoginModal(): void {
    this.bsModalRef = this.bsModalService.show(LoginModalComponent);
  }

  logoutUser(): void {
    this.authService.logOut();
    this.isLoggedIn = this.authService.isLoggedIn;
    this.router.navigate(['/home']);
    this.hideModal();
  }

  onLogout(): void {
    this.bsModalRef = this.bsModalService.show(LogoutModalComponent, {
      initialState: {
        message: 'Loggin\' out'
      }
    });
  }

  hideModal(): void {
    this.bsModalRef.hide();
  }

}
