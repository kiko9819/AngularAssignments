import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AnimalsComponent} from '../animals/animals.component';
import {PageNotFoundComponent} from '../page-not-found/page-not-found.component';
import {AuthGuard} from '../auth/auth-guard.service';
import {HomeComponent} from '../home/home.component';

const ROUTES: Routes = [
  {path: '', redirectTo: '/home', pathMatch: 'full'},
  {path: 'home', component: HomeComponent},
  {path: 'animals', canActivate: [AuthGuard], component: AnimalsComponent},
  {path: 'page-not-found', component: PageNotFoundComponent},
  {path: '**', redirectTo: '/page-not-found'},
];

@NgModule({
  imports: [RouterModule.forRoot(ROUTES)],
  exports: [RouterModule]
})
export class AppRoutesModule {
}
