import {Component, OnInit} from '@angular/core';
import {ModalNotifyService} from '../modal-notify.service';

@Component({
  selector: 'app-logout-modal',
  templateUrl: './logout-modal.component.html',
  styleUrls: ['./logout-modal.component.css']
})
export class LogoutModalComponent implements OnInit {

  constructor(private modalNotificaitonService: ModalNotifyService) {
  }

  ngOnInit() {
  }

  confirm(): void {
    this.modalNotificaitonService.onLogoutConfirmed.next();
  }

  decline(): void {
    this.modalNotificaitonService.onLogoutDeclined.next();
  }

}
