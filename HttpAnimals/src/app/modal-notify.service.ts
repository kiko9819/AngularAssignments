import { Injectable } from '@angular/core';
import {Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ModalNotifyService {
  onLoginConfirmed = new Subject<void>();
  onLoginCanceled = new Subject<void>();

  onLogoutConfirmed = new Subject<void>();
  onLogoutDeclined = new Subject<void>();

  constructor() { }
}
