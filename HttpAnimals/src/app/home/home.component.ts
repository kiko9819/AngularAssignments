import {Component, OnInit} from '@angular/core';
import {AuthService} from '../auth/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  message = this.authService.isLoggedIn ? 'You are logged in' : 'You need to login first.';

  constructor(private authService: AuthService) {
  }

  ngOnInit() {

  }

}
