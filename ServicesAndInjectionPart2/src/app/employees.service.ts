import {Injectable} from '@angular/core';
import {Employee} from './employee';

@Injectable({
  providedIn: 'root'
})
export class EmployeesService {
  employees: Employee[] = [];

  constructor() {
  }

  addToTable(employeeData: Employee) {
    this.employees.push(employeeData);
  }

  removeEmployee(index) {
    this.employees.splice(index, 1);
  }

  updateEmployee(newEmployee) {
    const updateEmployee = this.employees.find(this.findIndexToUpdate, newEmployee.id);
    const index = this.employees.findIndex((employee) => updateEmployee.id === employee.id);
    this.employees[index] = newEmployee;
  }

  findIndexToUpdate(newEmployee) {
    return newEmployee.id === this;
  }
}
