export class Employee {
    public id;
    public name;
    public email;
    public location;
    public phoneNumber;
    public department;
    public position;

    constructor(
      id: number,
      name: string,
      email: string,
      location: string,
      phoneNumber: string,
      department: string,
      position: string
    ) {
      this.id = id;
      this.name = name;
      this.email = email;
      this.location = location;
      this.phoneNumber = phoneNumber;
      this.department = department;
      this.position = position;
    }
  }
