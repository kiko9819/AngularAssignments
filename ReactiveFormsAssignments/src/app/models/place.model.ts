export class Place {
  public id: number;
  public name: string;
  public city: string;
  public rating: number;
  public imgPath: string;
  public description: string;

  constructor(id: number, name: string, city: string, rating: number, imgPath: string, description: string) {
    this.id = id;
    this.name = name;
    this.city = city;
    this.rating = rating;
    this.imgPath = imgPath;
    this.description = description;
  }
}
