import {Component, OnDestroy, OnInit, SimpleChanges} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CustomValidators} from './custom-validators';
import {PlacesService} from '../services/places.service';
import {Subscription} from 'rxjs';
import {Place} from '../models/place.model';

@Component({
  selector: 'app-places-form',
  templateUrl: './places-form.component.html',
  styleUrls: ['./places-form.component.css']
})
export class PlacesFormComponent implements OnInit, OnDestroy {
  place: Place;
  places: Place[];
  placeId: number;
  placesForm: FormGroup;

  forbiddenWords: string[];
  forbiddenWordsSubs: Subscription;

  constructor(private formBuilder: FormBuilder, private placesService: PlacesService) {
  }

  ngOnInit(): void {
    this.getForbiddenWords();
  }

  ngOnDestroy(): void {
    this.forbiddenWordsSubs.unsubscribe();
  }

  onSubmit(): void {
    this.createNewPlace();
    this.placesService.postPlace(this.place).subscribe(() => {
      this.placesService.onCreatePlace.next();
    });
    this.placesForm.reset();
  }

  private buildForm(): void {
    this.placesForm = this.formBuilder.group({
      'names': this.formBuilder.group({
        'placeName': ['',
          [
            Validators.required,
            Validators.minLength(2),
            Validators.maxLength(20),
            CustomValidators.validName
          ]
        ],
        'cityName': ['', [Validators.required, Validators.minLength(2)]],
      }),
      'rating': [0, CustomValidators.validRating],
      'imgPath': ['', CustomValidators.validUrl],
      'description': ['', [], CustomValidators.asyncValidDescription(this.forbiddenWords)]
    });
  }

  private getForbiddenWords(): void {
    this.forbiddenWordsSubs = this.placesService.getForbiddenWords()
      .subscribe((words) => {
        this.forbiddenWords = words;
        this.buildForm();
      });
  }

  private createNewPlace(): void {
    this.place = new Place(
      this.placeId++,
      this.placesForm.get('names.placeName').value,
      this.placesForm.get('names.cityName').value,
      this.placesForm.get('rating').value,
      this.placesForm.get('imgPath').value,
      this.placesForm.get('description').value,
    );
  }
}

