import {AbstractControl, FormControl, ValidatorFn, Validators} from '@angular/forms';
import {Observable} from 'rxjs';

export class CustomValidators {

  constructor() {
  }

  static validName(control: FormControl): { [key: string]: boolean } {
    const namePattern = /^[a-z\d\-_\s]+$/i;
    const namePatternIsValid = namePattern.test(control.value);

    return !namePatternIsValid ? {'namePatternIsValid': false} : null;
  }

  static validUrl(control: FormControl): { [key: string]: boolean } {
    const urlPattern = new RegExp('^(http|https|ftp)\://' +
      '([a-zA-Z0-9\.\-]+(\:[a-zA-Z0-9\.&amp;%\$\-]+)*@)*((25[0-5]' +
      '|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9])\.' +
      '(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.' +
      '(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.' +
      '(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])|' +
      '([a-zA-Z0-9\-]+\.)*[a-zA-Z0-9\-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|' +
      '[a-zA-Z]{2}))(\:[0-9]+)*(/($|[a-zA-Z0-9\.\,\?\'\\\+&amp;%\$#\=~_\-]+))*$');
    // console.log(urlPattern.test(control.value));
    return !urlPattern.test(control.value) ? {'urlIsInvalid': true} : null;
  }

  static validRating(control: FormControl): { [key: string]: boolean } {
    let ratingIsValid = true;

    if (control.value !== 0) {
      ratingIsValid = control.value >= 2 && control.value <= 10;
    }

    if (ratingIsValid) {
      return null;
    }
    return {
      'ratingIsValid': false
    };
  }

  static asyncValidDescription(forbiddenWords: string[]) {
    return (control: AbstractControl): Promise<any> | Observable<any> => {
      console.log(control.value.split(' '));
      return new Promise(
        (resolve, reject) => {
          setTimeout(() => {
            if (this.containsForbiddenWords(forbiddenWords, control.value.split(' '))) {
              resolve({'descriptionIsInvalid': true});
            } else {
              resolve(null);
            }
          }, 1000);
        }
      );
    };
  }

  static containsForbiddenWords(forbiddenWords: string[], controls: string[]): boolean {
    for (let i = 0; i < controls.length; i++) {
      for (let j = 0; j < forbiddenWords.length; j++) {
        if (forbiddenWords[j] === controls[i]) {
          console.log(controls[i]);
          return true;
        }
      }
    }
    return false;
  }
}
