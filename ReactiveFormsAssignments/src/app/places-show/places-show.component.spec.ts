import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlacesShowComponent } from './places-show.component';

describe('PlacesShowComponent', () => {
  let component: PlacesShowComponent;
  let fixture: ComponentFixture<PlacesShowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlacesShowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlacesShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
