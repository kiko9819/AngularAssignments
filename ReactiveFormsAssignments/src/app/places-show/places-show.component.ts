import {Component, OnDestroy, OnInit} from '@angular/core';
import {PlacesService} from '../services/places.service';
import {Place} from '../models/place.model';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-places-show',
  templateUrl: './places-show.component.html',
  styleUrls: ['./places-show.component.css']
})
export class PlacesShowComponent implements OnInit, OnDestroy {
  places: Place[];
  placesSubscription: Subscription;

  constructor(private placesService: PlacesService) {
  }

  ngOnInit(): void {
    this.getPlaces();
    this.placesSubscription = this.placesService.onCreatePlace.subscribe(() => {
      this.getPlaces();
    });
  }

  ngOnDestroy(): void {
    if (this.placesSubscription) {
      this.placesSubscription.unsubscribe();
    }
  }

  getPlaces(): void {
    this.placesService.getPlaces().subscribe((places) => {
      this.places = places;
    });
  }
}
