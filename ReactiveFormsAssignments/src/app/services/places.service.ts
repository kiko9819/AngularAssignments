import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {Place} from '../models/place.model';

@Injectable({
  providedIn: 'root'
})
export class PlacesService {
  onCreatePlace = new Subject();

  constructor(private httpClient: HttpClient) {
  }

  getForbiddenWords(): Observable<string[]> {
    return this.httpClient.get<string[]>('http://localhost:3000/forbiddenWords');
  }

  getPlaces(): Observable<Place[]> {
    return this.httpClient.get<Place[]>('http://localhost:3000/places');
  }

  postPlace(newPlace: Place): Observable<Place> {
    return this.httpClient.post<Place>('http://localhost:3000/places', newPlace);
  }
}
