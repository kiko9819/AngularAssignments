import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PlacesShowComponent} from '../places-show/places-show.component';

const ROUTES: Routes = [
  {path: '', redirectTo: '/new-places', pathMatch: 'full'},
  {path: 'new-places', component: PlacesShowComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(ROUTES)],
  exports: [RouterModule]
})
export class AppRoutesModule {
}
