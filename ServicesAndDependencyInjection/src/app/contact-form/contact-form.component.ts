import {Component, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {Validators, FormBuilder, FormGroup} from '@angular/forms';

import {Contact} from '../contact';
import {ContactsService} from '../contacts.service';

@Component({
  selector: 'app-contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.css']
})
export class ContactFormComponent implements OnInit {
  contactForm: FormGroup;

  @Output() onContactAdded = new EventEmitter<Contact>();
  @ViewChild('firstName') firstName;
  @ViewChild('lastName') lastName;
  @ViewChild('phoneNumber') phoneNumber;
  @ViewChild('email') email;
  @ViewChild('city') city;

  constructor(private fb: FormBuilder, private contactsService: ContactsService) {
  }

  ngOnInit() {
    this.contactForm = this.fb.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      phoneNumber: ['', Validators.compose([Validators.required, Validators.minLength(10)])],
      email: ['', Validators.compose([Validators.required, Validators.email])],
      city: ['', Validators.required]
    });
  }

  onSubmit(event) {
    event.preventDefault();
    if (this.contactForm.invalid) {
      return;
    }
    this.contactsService.pushContact(
      new Contact(
        this.firstName.nativeElement.value,
        this.lastName.nativeElement.value,
        this.phoneNumber.nativeElement.value,
        this.email.nativeElement.value,
        this.city.nativeElement.value
      )
    );
    this.contactForm.reset();
  }
}
