import {Injectable} from '@angular/core';
import {Contact} from './contact';

@Injectable({
  providedIn: 'root'
})
export class ContactsService {
  contacts: Contact[] = [
    new Contact(
      'Leanne',
      'Graham',
      '1-770-736-8031 x56442',
      'Sincere@april.biz',
      'Gwenborough')
  ];

  constructor() {
  }

  pushContact(data: Contact) {
    this.contacts.push(data);
  }
}
