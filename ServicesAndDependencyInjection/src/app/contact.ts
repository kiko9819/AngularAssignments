export class Contact {
  public firstName;
  public lastName;
  public phoneNumber;
  public email;
  public city;

  constructor(firstName: string, lastName: string, phoneNumber: string, email: string, city: string) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.phoneNumber = phoneNumber;
    this.email = email;
    this.city = city;
  }
}
