import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {WelcomeComponent} from '../header/welcome/welcome.component';
import {AboutComponent} from '../header/about/about.component';
import {AlbumComponent} from '../header/album/album.component';
import {PageNotFoundComponent} from '../page-not-found/page-not-found.component';

const appRoutes: Routes = [
  {path: '', redirectTo: '/home', pathMatch: 'full'},
  {path: 'home', component: WelcomeComponent},
  {path: 'about', component: AboutComponent},
  {path: 'album', component: AlbumComponent},
  {path: 'not-found', component: PageNotFoundComponent},
  {path: '**', redirectTo: '/not-found'}
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutesModule {
}
