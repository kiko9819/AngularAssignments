import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';

import {PostsService} from '../posts/services/posts.service';
import {UsersService} from '../users/services/users.service';

@Component({
  selector: 'app-delete-confirm-modal',
  templateUrl: './delete-confirm-modal.component.html',
  styleUrls: ['./delete-confirm-modal.component.css']
})
export class DeleteConfirmModalComponent implements OnInit, OnDestroy {
  postDeleteSubscription: Subscription;
  userDeleteSubscription: Subscription;

  postId: number;
  userId: number;
  message: string;

  constructor(private postsService: PostsService, private usersService: UsersService) {
  }

  ngOnInit(): void {
    this.postDeleteSubscription = this.postsService.onPostDeleteModal.subscribe(postId => {
      this.postId = postId;
    });
    this.userDeleteSubscription = this.usersService.onUserDeleteModal.subscribe(userId => {
      this.userId = userId;
    });
  }

  ngOnDestroy(): void {
    if (this.postDeleteSubscription) {
      this.postDeleteSubscription.unsubscribe();
    }
    if (this.userDeleteSubscription) {
      this.userDeleteSubscription.unsubscribe();
    }
  }

  confirm(): void {
    if (this.userId) {
      this.usersService.deleteUser(this.userId).subscribe(() => {
        this.usersService.onUserDeleteConfirmed.next();
      });
    } else {
      this.postDeleteSubscription = this.postsService.deletePost(this.postId).subscribe(
        () => {
          this.postsService.onPostDeleteConfirmed.next();
        });
    }
  }

  decline(): void {
    if (this.userDeleteSubscription) {
      this.usersService.onUserDeleteDeclined.next();
    } else {
      this.postsService.onPostDeleteDeclined.next();
    }
  }

}
