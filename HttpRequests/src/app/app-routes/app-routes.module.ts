import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {UsersComponent} from '../users/users.component';
import {PostsComponent} from '../posts/posts.component';
import {NotFoundComponent} from '../not-found/not-found.component';
import {UserComponent} from '../users/user/user.component';
import {PostComponent} from '../posts/post/post.component';

const ROUTES: Routes = [
  {path: '', redirectTo: '/users', pathMatch: 'full'},
  {
    path: 'users', component: UsersComponent, children: [
      {path: ':id', component: UserComponent}
    ]
  },
  {
    path: 'posts', component: PostsComponent, children: [
      {path: ':id', component: PostComponent}
    ]
  },
  {path: 'not-found', component: NotFoundComponent},
  {path: '**', redirectTo: '/not-found'}
];

@NgModule({
  imports: [RouterModule.forRoot(ROUTES)],
  exports: [RouterModule]
})
export class AppRoutesModule {
}
