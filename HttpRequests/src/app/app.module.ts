import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {ReactiveFormsModule} from '@angular/forms';
import {AccordionModule, CollapseModule, ModalModule} from 'ngx-bootstrap';

import {AppComponent} from './app.component';
import {HeaderComponent} from './header/header.component';
import {UsersComponent} from './users/users.component';
import {UsersListComponent} from './users/users-list/users-list.component';
import {PostsComponent} from './posts/posts.component';
import {PostsListComponent} from './posts/posts-list/posts-list.component';
import {EditComponent} from './edit-modal/edit-modal.component';
import {AppRoutesModule} from './app-routes/app-routes.module';
import {NotFoundComponent} from './not-found/not-found.component';
import {UserComponent} from './users/user/user.component';
import {PostComponent} from './posts/post/post.component';
import { DeleteConfirmModalComponent } from './delete-confirm-modal/delete-confirm-modal.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    UsersComponent,
    UsersListComponent,
    PostsComponent,
    PostsListComponent,
    NotFoundComponent,
    EditComponent,
    UserComponent,
    PostComponent,
    DeleteConfirmModalComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutesModule,
    HttpClientModule,
    ReactiveFormsModule,
    ModalModule.forRoot(),
    CollapseModule.forRoot(),
    AccordionModule.forRoot()
  ],
  entryComponents: [
    EditComponent,
    UserComponent,
    PostComponent,
    DeleteConfirmModalComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
