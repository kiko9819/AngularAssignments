import {Component, OnDestroy, OnInit, TemplateRef} from '@angular/core';
import {User} from '../models/users.model';
import {UsersService} from '../services/users.service';
import {Subscription} from 'rxjs';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {EditComponent} from '../../edit-modal/edit-modal.component';
import {UserComponent} from '../user/user.component';
import {initialState} from 'ngx-bootstrap/timepicker/reducer/timepicker.reducer';
import {Geo} from '../models/geo.model';
import {Address} from '../models/address.model';
import {DeleteConfirmModalComponent} from '../../delete-confirm-modal/delete-confirm-modal.component';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.css']
})
export class UsersListComponent implements OnInit, OnDestroy {
  users: User[];
  usersSubscription: Subscription;

  userShowBsModalRef: BsModalRef;
  userEditBsModalRef: BsModalRef;
  userDeleteBsModalRef: BsModalRef;

  userSaveSubscription: Subscription;
  userCloseSubscription: Subscription;
  userDeleteConfirmedSubscription: Subscription;
  userDeleteDeclinedSubscription: Subscription;

  constructor(private usersService: UsersService, private bsModalService: BsModalService) {
  }

  ngOnInit(): void {
    this.getUsers();
    this.userDeleteConfirmedSubscription = this.usersService.onUserDeleteConfirmed.subscribe(() => {
      this.getUsers();
      this.userDeleteBsModalRef.hide();
    });
    this.userDeleteDeclinedSubscription = this.usersService.onUserDeleteDeclined.subscribe(() => {
      this.getUsers();
      this.userDeleteBsModalRef.hide();
    });
  }

  ngOnDestroy(): void {
    if (this.usersSubscription) {
      this.usersSubscription.unsubscribe();
    }
    if (this.userSaveSubscription) {
      this.userSaveSubscription.unsubscribe();
    }
    if (this.userCloseSubscription) {
      this.userCloseSubscription.unsubscribe();
    }
  }

  getUsers(): void {
    this.usersSubscription = this.usersService.getUsers()
      .subscribe((users) => {
        this.users = users;
      });
  }

  onEditUser(user?: User) {
    if (!user) {
      user = new User();
    }
    this.userEditBsModalRef = this.bsModalService.show(
      EditComponent, {
        initialState: {
          user: user
        },
        class: 'gray modal-lg'
      });

    this.userSaveSubscription = this.usersService.onUserSave.subscribe(() => {
      this.getUsers();
    });

    this.userCloseSubscription = this.usersService.onUserModalClose.subscribe(() => {
    });
  }

  onDeleteModal(userId: number): void {
    this.userDeleteBsModalRef = this.bsModalService.show(DeleteConfirmModalComponent, {
      initialState: {
        message: `Deleting user № ${userId}`
      }
    });
    this.usersService.onUserDeleteModal.next(userId);
  }

  onSeeMore(user: User): void {
    this.usersService.onUserShow.next(user);
    this.userShowBsModalRef = this.bsModalService.show(UserComponent,
      {
        initialState: {
          user: user,
        }
      }
    );
  }
}
