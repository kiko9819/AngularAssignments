import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, Subject} from 'rxjs';
import {User} from '../models/users.model';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  onUserSave = new Subject<User>();
  onUserModalClose = new Subject<void>();
  onUserShow = new Subject<User>();

  onUserDeleteModal = new Subject<number>();
  onUserDeleteConfirmed = new Subject<void>();
  onUserDeleteDeclined = new Subject<void>();

  constructor(private httpClient: HttpClient) {
  }

  getUsers(): Observable<User[]> {
    // return this.httpClient.get<User[]>('https://jsonplaceholder.typicode.com/users/');
    return this.httpClient.get<User[]>('http://localhost:3000/users');
  }

  // getUser():

  postUser(user: User): Observable<User> {
    // return this.httpClient.post<User>('https://jsonplaceholder.typicode.com/users/', user);
    return this.httpClient.post<User>('http://localhost:3000/users', user);
  }

  putUser(user: User): Observable<User> {
    return this.httpClient.put<User>('http://localhost:3000/users/' + user.id, user);
  }

  deleteUser(userId: number): Observable<undefined> {
    return this.httpClient.delete<undefined>('http://localhost:3000/users/' + userId);
  }

  saveUser(user: User): Observable<User> {
    if (!user.id) {
      return this.postUser(user);
    } else {
      console.log('has id');
      return this.putUser(user);
    }
  }


}
