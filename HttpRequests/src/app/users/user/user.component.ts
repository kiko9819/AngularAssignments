import {Component, OnDestroy, OnInit} from '@angular/core';
import {UsersService} from '../services/users.service';
import {Subscription} from 'rxjs';
import {User} from '../models/users.model';
import {BsModalRef} from 'ngx-bootstrap';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit, OnDestroy {
  user: User;
  onUserShowSubscription: Subscription;

  constructor(private usersService: UsersService, private bsModalRef: BsModalRef) {
  }

  ngOnInit(): void {
    this.onUserShowSubscription = this.usersService.onUserShow.subscribe((user: User) => {
      console.log(user);
    });
  }

  ngOnDestroy(): void {
    if (this.onUserShowSubscription) {
      this.onUserShowSubscription.unsubscribe();
    }
  }

  onHideModal(): void {
    this.bsModalRef.hide();
  }

}
