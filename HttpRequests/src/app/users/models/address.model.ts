import {Geo} from './geo.model';

export class Address {
  street: string;
  suite: string;
  city: string;
  zipCode: string;
  geo: Geo;

  constructor(street?: string,
              suite?: string,
              city?: string,
              zipCode?: string,
              geo?: Geo) {
    this.street = street;
    this.suite = suite;
    this.city = city;
    this.zipCode = zipCode;
    this.geo = geo;
  }
}
