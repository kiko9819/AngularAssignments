import {AbstractControl, ValidatorFn} from '@angular/forms';

export class CustomValidators {
  static validateSuite(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: boolean } | null => {
      if (control.value) {
        return control.value.toString().length >= 4 ? null : {'suiteIsInvalid': true};
      }
    };
  }

  static validateLatitude(): ValidatorFn {
    const latLowerLimit = -90;
    const latUpperLimit = 90;
    return (control: AbstractControl): { [key: string]: boolean } => {
      if (control.value) {
        const validLatitude = control.value > latLowerLimit && control.value < latUpperLimit;
        return validLatitude ? null : {'latitudeIsInvalid': true};
      }
    };
  }

  static validateLongitude(): ValidatorFn {
    const lngLowerLimit = -180;
    const lngUpperLimit = 180;
    return (control: AbstractControl): { [key: string]: boolean } => {
      if (control.value) {
        const validLongitude = control.value > lngLowerLimit && control.value < lngUpperLimit;
        return validLongitude ? null : {'longitudeIsInvalid': true};
      }
    };
  }

  // static validateUserNameNotTaken(usersService: UsersService) {
  //   return (control: AbstractControl) => {
  //
  //   };
  // }

}
