import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {BsModalRef} from 'ngx-bootstrap';
import {UsersService} from '../users/services/users.service';
import {PostsService} from '../posts/services/posts.service';
import {Router} from '@angular/router';
import {CustomValidators} from './custom.validators';
import {User} from '../users/models/users.model';
import {Post} from '../posts/models/posts.model';

@Component({
  selector: 'app-edit',
  templateUrl: './edit-modal.component.html',
  styleUrls: ['./edit-modal.component.css']
})
export class EditComponent implements OnInit {
  currentUrl;
  user: User;
  post: Post;

  userFormGroup: FormGroup;
  postFormGroup: FormGroup;

  constructor(private formBuilder: FormBuilder,
              private bsModalRef: BsModalRef,
              private usersService: UsersService,
              private postsService: PostsService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.currentUrl = this.router.url;
    if (this.currentUrl === '/users') {
      this.buildUserForm();
    } else {
      this.buildPostForm();
    }
  }

  onSubmit(): void {
    if (this.currentUrl === '/users') {
      const userData = this.userFormGroup.value;
      this.usersService.saveUser(userData).subscribe((user) => {
        this.usersService.onUserSave.next(user);
        this.bsModalRef.hide();
      });
    } else {
      const postData = this.postFormGroup.value;
      this.postsService.savePost(postData).subscribe((post) => {
        this.postsService.onPostSave.next(post);
        this.bsModalRef.hide();
      });
    }

  }

  private buildUserForm(): void {
    this.userFormGroup = this.formBuilder.group({
      id: this.user.id,
      name: [this.user.name,
        [Validators.required, Validators.minLength(5)], /*CustomValidators.validateUserNameNotTaken(this.usersService)*/
      ],
      username: [this.user.username, Validators.required],
      email: [this.user.email, [Validators.required, Validators.email]],
      address: this.formBuilder.group({
        street: [this.user.address.street, Validators.required],
        suite: [this.user.address.suite, CustomValidators.validateSuite()],
        city: [this.user.address.city, [Validators.required, Validators.minLength(5)]],
        zipCode: [this.user.address.zipCode, [Validators.required, Validators.minLength(4)]],
        geo: this.formBuilder.group({
          lat: [this.user.address.geo.lat, CustomValidators.validateLatitude()],
          lng: [this.user.address.geo.lng, CustomValidators.validateLongitude()]
        })
      }),
      company: this.formBuilder.group({
        name: [this.user.company.name, Validators.required],
        catchPhrase: [this.user.company.catchPhrase, Validators.required],
        bs: [this.user.company.bs, Validators.required],
      }),
      phone: [this.user.phone, [Validators.required, Validators.minLength(10)]],
      website: [this.user.website]
    });
  }

  private buildPostForm(): void {
    this.postFormGroup = this.formBuilder.group({
      userId: this.post.userId,
      id: this.post.id,
      title: [this.post.title, Validators.required],
      body: [this.post.body, [Validators.required, Validators.minLength(10)]]
    });
  }

  onHideModal() {
    this.bsModalRef.hide();
    if (this.currentUrl === '/users') {
      this.usersService.onUserModalClose.next();
    } else {
      this.postsService.onPostModalClose.next();
    }
  }

}
