import {Component, OnDestroy, OnInit} from '@angular/core';
import {EditComponent} from '../../edit-modal/edit-modal.component';
import {Subscription} from 'rxjs';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {Post} from '../models/posts.model';
import {PostsService} from '../services/posts.service';
import {initialState} from 'ngx-bootstrap/timepicker/reducer/timepicker.reducer';
import {DeleteConfirmModalComponent} from '../../delete-confirm-modal/delete-confirm-modal.component';

@Component({
  selector: 'app-posts-list',
  templateUrl: './posts-list.component.html',
  styleUrls: ['./posts-list.component.css']
})
export class PostsListComponent implements OnInit, OnDestroy {

  posts: Post[];
  postsSubscription: Subscription;

  postEditBsModalRef: BsModalRef;
  postDeleteBsModalRef: BsModalRef;

  postSaveSubscription: Subscription;
  postCloseSubscription: Subscription;
  postDeleteSubscription: Subscription;
  postDeleteConfirmedSubscription: Subscription;
  postDeleteDeclinedSubscription: Subscription;

  constructor(private postsService: PostsService, private bsModalService: BsModalService) {
  }

  ngOnInit(): void {
    this.getPosts();
    this.postDeleteConfirmedSubscription = this.postsService.onPostDeleteConfirmed.subscribe(() => {
      this.getPosts();
      this.postDeleteBsModalRef.hide();
    });
    this.postDeleteDeclinedSubscription = this.postsService.onPostDeleteDeclined.subscribe(() => {
      this.postDeleteBsModalRef.hide();
    });
  }

  ngOnDestroy(): void {
    if (this.postsSubscription) {
      this.postsSubscription.unsubscribe();
    }
    if (this.postSaveSubscription) {
      this.postSaveSubscription.unsubscribe();
    }
    if (this.postCloseSubscription) {
      this.postCloseSubscription.unsubscribe();
    }
    if (this.postDeleteSubscription) {
      this.postDeleteSubscription.unsubscribe();
    }
    if (this.postDeleteConfirmedSubscription) {
      this.postDeleteConfirmedSubscription.unsubscribe();
    }
    if (this.postDeleteDeclinedSubscription) {
      this.postDeleteDeclinedSubscription.unsubscribe();
    }
  }

  getPosts(): void {
    this.postsSubscription = this.postsService.getPosts()
      .subscribe((posts) => {
        this.posts = posts;
      });
  }

  onEditPost(post?: Post): void {
    if (!post) {
      post = new Post();
    }

    this.postEditBsModalRef = this.bsModalService.show(EditComponent, {
      initialState: {
        post: post
      },
      class: 'gray modal-lg'
    });

    this.postSaveSubscription = this.postsService.onPostSave.subscribe(() => {
      this.getPosts();
    });

    this.postCloseSubscription = this.postsService.onPostModalClose.subscribe(() => {
    });
  }

  onDeleteConfirmation(postId: number): void {
    this.postDeleteBsModalRef = this.bsModalService.show(DeleteConfirmModalComponent, {
      initialState: {
        message: `Deleting post № ${postId}`
      }
    });
    this.postsService.onPostDeleteModal.next(postId);
  }

}
