import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {Post} from '../models/posts.model';
import {HttpClient} from '@angular/common/http';
import {BsModalRef} from 'ngx-bootstrap';

@Injectable({
  providedIn: 'root'
})
export class PostsService {
  onPostSave = new Subject<Post>();
  onPostModalClose = new Subject<void>();
  onPostDeleteModal = new Subject<number>();
  onPostDeleteConfirmed = new Subject<void>();
  onPostDeleteDeclined = new Subject<void>();

  constructor(private httpClient: HttpClient) {
  }

  getPosts(): Observable<Post[]> {
    // return this.httpClient.get<User[]>('https://jsonplaceholder.typicode.com/users/');
    return this.httpClient.get<Post[]>('http://localhost:3000/posts');
  }

  createPost(post: Post): Observable<Post> {
    // return this.httpClient.post<User>('https://jsonplaceholder.typicode.com/users/', user);
    return this.httpClient.post<Post>('http://localhost:3000/posts', post);
  }

  putPost(post: Post): Observable<Post> {
    return this.httpClient.put<Post>('http://localhost:3000/posts/' + post.id, post);
  }

  deletePost(postId: number): Observable<undefined> {
    // console.log(postId);
    return this.httpClient.delete<undefined>('http://localhost:3000/posts/' + postId);
  }

  savePost(post: Post): Observable<Post> {
    if (!post.id) {
      return this.createPost(post);
    } else {
      return this.putPost(post);
    }
  }
}
