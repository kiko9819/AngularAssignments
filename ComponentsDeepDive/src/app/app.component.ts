import {Component} from '@angular/core';
import {Contact} from './contact';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  contacts: Contact[] = [
    new Contact(
      'Leanne',
      'Graham',
      '1-770-736-8031 x56442',
      'Sincere@april.biz',
      'Gwenborough')
  ];

  pushContact(data: Contact) {
    this.contacts.push(data);
  }
}
