import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-colored-paragraphs',
  templateUrl: './colored-paragraphs.component.html',
  styleUrls: ['./colored-paragraphs.component.css']
})
export class ColoredParagraphsComponent implements OnInit {
  selectedColor = 'default';

  constructor() {
  }

  ngOnInit() {

  }

}
