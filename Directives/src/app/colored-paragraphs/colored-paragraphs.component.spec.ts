import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ColoredParagraphsComponent } from './colored-paragraphs.component';

describe('ColoredParagraphsComponent', () => {
  let component: ColoredParagraphsComponent;
  let fixture: ComponentFixture<ColoredParagraphsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ColoredParagraphsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ColoredParagraphsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
