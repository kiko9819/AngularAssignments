import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-gender-select',
  templateUrl: './gender-select.component.html',
  styleUrls: ['./gender-select.component.css']
})
export class GenderSelectComponent implements OnInit {
  gender = 'default';
  imgUrl = '../assets/default.jpeg';
  genderType = '';
  otherGender = '';

  constructor() {
  }

  ngOnInit() {

  }

  onPictureChange() {
    if (this.gender === 'default' || this.gender === 'other') {
      this.genderType = 'default';
    } else if (this.gender === 'male') {
      this.genderType = 'male';
    } else {
      this.genderType = 'female';
    }
    this.imgUrl = `../assets/${this.genderType}.jpeg`;
  }

  onCharInput() {
    if (this.otherGender === 'alien') {
      this.imgUrl = '../assets/alien.jpeg';
    } else {
      this.imgUrl = '../assets/default.jpeg';
    }
  }

}


