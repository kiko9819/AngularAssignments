import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-img-rounded',
  templateUrl: './img-rounded.component.html',
  styleUrls: ['./img-rounded.component.css']
})
export class ImgRoundedComponent implements OnInit {
  letters;
  lettersLength;

  constructor() {
  }

  ngOnInit() {
  }

  onKeyPressLog() {
    this.lettersLength = this.letters.length;
  }

  getLettersCount(){
    return this.lettersLength>10 ? 'rounded' : 'non-rounded';
  }
}
