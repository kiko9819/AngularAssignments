import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CompareComponent } from './compare/compare.component';
import {FormsModule} from '@angular/forms';
import { HiddenContentComponent } from './hidden-content/hidden-content.component';
import { UserAdminComponent } from './user-admin/user-admin.component';
import { GenderSelectComponent } from './gender-select/gender-select.component';
import { ColoredParagraphsComponent } from './colored-paragraphs/colored-paragraphs.component';
import { ImgRoundedComponent } from './img-rounded/img-rounded.component';
import { NamesComponent } from './names/names.component';
import { DateLogsComponent } from './date-logs/date-logs.component';

@NgModule({
  declarations: [
    AppComponent,
    CompareComponent,
    HiddenContentComponent,
    UserAdminComponent,
    GenderSelectComponent,
    ColoredParagraphsComponent,
    ImgRoundedComponent,
    NamesComponent,
    DateLogsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
