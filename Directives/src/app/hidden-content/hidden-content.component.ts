import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-hidden-content',
  templateUrl: './hidden-content.component.html',
  styleUrls: ['./hidden-content.component.css']
})
export class HiddenContentComponent implements OnInit {
  isVisible = false;

  constructor() {
  }

  ngOnInit() {
  }

}
