import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-date-logs',
  templateUrl: './date-logs.component.html',
  styleUrls: ['./date-logs.component.css']
})
export class DateLogsComponent implements OnInit {
  logs = [];
  counter = 0;

  constructor() {
  }

  ngOnInit() {
    this.seedLogs();
  }

  logCurrentDate() {
    this.logs.push(new Date().toLocaleString());
  }

  private seedLogs() {
    let interval = setInterval(() => {
      this.logs.push(new Date().toLocaleString());
      if (++this.counter === 3) {
        clearInterval(interval);
      }
    }, 1000);
  };
}
