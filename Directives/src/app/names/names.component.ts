import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-names',
  templateUrl: './names.component.html',
  styleUrls: ['./names.component.css']
})
export class NamesComponent implements OnInit {
  newName = '';
  nameTooShort = false;

  names = [
    'Leanne Graham',
    'Ervin Howell',
    'Clementine Bauch',
    'Patricia Lebsack',
    'Chelsey Dietrich',
    'Mrs. Dennis Schulist',
    'Kurtis Weissnat',
    'Nicholas Runolfsdottir V',
    'Glenna Reichert',
    'Clementina DuBuque'
  ];

  constructor() {
  }

  ngOnInit() {
  }

  checkName() {
    if (this.newName.length <= 1) {
      this.nameTooShort = true;
    } else {
      this.addName(this.newName);
    }
  }

  onKeyLog() {
    if (this.newName.length > 1) {
      this.nameTooShort = false;
    }
  }

  addName(newName: string) {
    this.names.push(newName);
  }

}
