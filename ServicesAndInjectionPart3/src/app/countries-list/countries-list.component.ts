import {Component, OnInit} from '@angular/core';
import {Country} from './country.model';
import {CountriesService} from './countries.service';

@Component({
  selector: 'app-countries-list',
  templateUrl: './countries-list.component.html',
  styleUrls: ['./countries-list.component.css']
})
export class CountriesListComponent implements OnInit {
  countries: Country[] = [];

  constructor(private countriesService: CountriesService) {
  }

  ngOnInit() {
    this.countries = this.countriesService.countriesTable;
  }

  removeCountry(id) {
    this.countriesService.deleteCountry(id);
  }
}
