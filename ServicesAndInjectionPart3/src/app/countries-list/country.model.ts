export class Country {
  public id: number;
  public name: string;
  public countryCode: string;
  public currency: string;
  public currencyCode: string;

  constructor(id: number, name: string, countryCode: string, currency: string, currencyCode: string) {
    this.id = id;
    this.name = name;
    this.countryCode = countryCode;
    this.currency = currency;
    this.currencyCode = currencyCode;
  }
}
