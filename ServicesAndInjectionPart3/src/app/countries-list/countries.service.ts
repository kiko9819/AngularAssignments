import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

import {Country} from './country.model';

@Injectable({
  providedIn: 'root'
})
export class CountriesService {
  countries: Country[] = [];
  countriesTable: Country[] = [];

  constructor(private http: HttpClient) {
    this.getJSON().subscribe(data => {
      this.countries = data;
    });
  }

  createCountry(newCountry: Country) {
    this.countriesTable.push(newCountry);
    // console.log(`Created new Country: ${this.countriesTable[newCountry.id]}`);
    console.log('Created');
    console.log(newCountry);
  }

  updateCountry(currentCountry: Country, newCountry) {
    const updateCountry = this.countriesTable.find(this.findIndexToUpdate, currentCountry.id);
    const index = this.countriesTable.findIndex((country) => updateCountry.id === country.id);
    this.countriesTable[index] = newCountry;
    console.log('Updated');
    console.log(currentCountry);
  }

  deleteCountry(countryId) {
    console.log('Deleted:');
    console.log(this.countriesTable[countryId]);
    this.countriesTable.splice(countryId, 1);
  }

  findIndexToUpdate(currentCountry) {
    return currentCountry.id === this;
  }

  getJSON(): Observable<any> {
    return this.http.get('../assets/countries.json');
  }
}
