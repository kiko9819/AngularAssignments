import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Country} from '../country.model';

import {CountriesService} from '../countries.service';

@Component({
  selector: 'app-edit-country',
  templateUrl: './edit-country.component.html',
  styleUrls: ['./edit-country.component.css']
})
export class EditCountryComponent implements OnInit {
  countryForm: FormGroup;
  countryId = 0;
  countries: Country[] = [];

  @ViewChild('lgModal') lgModal;

  @Input() editMode: boolean;
  @Input() currentlyEditedCountry: Country;
  @Input() buttonText: string;

  constructor(private formBuilder: FormBuilder, private countriesService: CountriesService) {
  }

  ngOnInit() {
    this.countryForm = this.formBuilder.group({
      name: ['', Validators.required],
      countryCode: ['', Validators.required],
      currency: ['', Validators.required],
      currencyCode: ['', Validators.required]
    });
    this.countriesService.getJSON().subscribe(data => this.countries = data);
  }

  sendCountryData(event) {
    event.preventDefault();
    if (this.countryForm.invalid) {
      return;
    }
    if (this.editMode) {
      this.countriesService.updateCountry(
        this.currentlyEditedCountry,
        this.generateNewCountry(this.currentlyEditedCountry.id)
      );
    } else {
      this.countriesService.createCountry(this.generateNewCountry());
    }
    this.lgModal.hide();
    this.countryForm.reset(this.countryForm);
  }

  generateNewCountry(countryId?: number): Country {
    const id = countryId || this.countryId++;
    return new Country(
      id,
      this.countryForm.controls['name'].value,
      this.countryForm.controls['countryCode'].value,
      this.countryForm.controls['currency'].value,
      this.countryForm.controls['currencyCode'].value
    );
  }

  showModal() {
    this.lgModal.show();
    if (this.editMode) {
      this.seedFormInputFields();
    }
  }

  hideModal() {
    this.lgModal.hide();
    this.editMode = false;
  }

  seedFormInputFields(): void {
    this.countryForm.controls['name'].setValue(this.currentlyEditedCountry.name);
    this.countryForm.controls['countryCode'].setValue(this.currentlyEditedCountry.countryCode);
    this.countryForm.controls['currency'].setValue(this.currentlyEditedCountry.currency);
    this.countryForm.controls['currencyCode'].setValue(this.currentlyEditedCountry.currencyCode);
  }
}

