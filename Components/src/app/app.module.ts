import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { PersonalInfoComponent } from './personal-info/personal-info.component';
import { WarningComponent } from './warning/warning.component';
import { SuccessComponent } from './success/success.component';
import { ImgGalleryComponent } from './img-gallery/img-gallery.component';
import { ImgBoxComponent } from './img-gallery/img-box/img-box.component';

@NgModule({
  declarations: [
    AppComponent,
    PersonalInfoComponent,
    WarningComponent,
    SuccessComponent,
    ImgGalleryComponent,
    ImgBoxComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
