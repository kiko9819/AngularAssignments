import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-personal-info',
  template:
      `
    <div id="person-container">
      <header class="personal-info">Personal Information</header>
      <div class="inner-wrapper">
        <div class="names">
          <span>Name: {{ name }}</span>
          <span>Surname: {{ surname }}</span>
        </div>
        <span>Age: {{ age }}</span>
        <div class="about">
          <h2>About me</h2>
          <p class="lead">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab accusantium aperiam aspernatur fuga harum laborum mollitia nam
            nulla
            odio quas.
          </p>
        </div>
      </div>
    </div>
  `,
  styles: [
    '.inner-wrapper { margin: 1rem; color: #17A3C8}',
    '#person-container {\n' +
    '  display: -webkit-box;\n' +
    '  display: -ms-flexbox;\n' +
    '  display: flex;\n' +
    '  -webkit-box-orient: vertical;\n' +
    '  -webkit-box-direction: normal;\n' +
    '  -ms-flex-direction: column;\n' +
    '  flex-direction: column;\n' +
    '  margin-left: auto;\n' +
    '  margin-right: auto;\n' +
    '  width: 23rem;\n' +
    '  font-family: sans-serif;\n' +
    '  border-top-left-radius: 0.3rem;\n' +
    '  border-top-right-radius: 0.3rem;\n' +
    '  border: 1px solid #17a2b8;\n' +
    'transition: all 0.3s;\n' +
    '}',
    '#person-container:hover{ box-shadow: 1px 1px 10px #17a2b8;\n' +
    '-webkit-transform: scale(1.01);\n' +
    'transform: scale(1.01);\n' +
    '-webkit-transition: all 0.3s;}',
    '.personal-info {font-size: 1.5rem; padding: .7rem;\n' +
    '  color: #fff;background-color: #17a2b8;\n' +
    '}',
    '.names {\n' +
    '  display: -webkit-box;\n' +
    '  display: -ms-flexbox;\n' +
    '  display: flex;\n' +
    '  -webkit-box-orient: vertical;\n' +
    '  -webkit-box-direction: normal;\n' +
    '  -ms-flex-direction: column;\n' +
    '  flex-direction: column;\n' +
    '}',
    'span { font-size: 1.3rem; line-height: 1.4;}',
    '.lead { padding: 1.3rem; margin: 0;}' +
    'letter-spacing: .05rem; line-height: 1.3;',
    'h2 { margin: 0;}',
    '.about {\n' +
    '  margin-top: 1rem;\n' +
    '}'

  ]
})
export class PersonalInfoComponent implements OnInit {
  name = 'Kristiyan';
  surname = 'Krastev';
  age = 20;

  constructor() {
  }

  ngOnInit() {
  }

}
