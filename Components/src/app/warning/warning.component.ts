import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-warning',
  templateUrl: './warning.component.html',
  styleUrls: ['./warning.component.css']
})
export class WarningComponent implements OnInit {
  warningMessage = `Oops...you forgot to save.`;

  constructor() {
  }

  ngOnInit() {
  }

}
