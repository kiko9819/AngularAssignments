import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-img-gallery',
  templateUrl: './img-gallery.component.html',
  styleUrls: ['./img-gallery.component.css']
})
export class ImgGalleryComponent implements OnInit {
  boxes = [
    {
      boxTitle: 'Image Box',
      boxPath: '../../assets/angularjs.png'
    },
    {
      boxTitle: 'Image Box',
      boxPath: '../../assets/angularjs.png'
    },
    {
      boxTitle: 'Image Box',
      boxPath: '../../assets/angularjs.png'
    },
    {
      boxTitle: 'Image Box',
      boxPath: '../../assets/angularjs.png'
    },
    {
      boxTitle: 'Image Box',
      boxPath: '../../assets/angularjs.png'
    },
    {
      boxTitle: 'Image Box',
      boxPath: '../../assets/angularjs.png'
    },
    {
      boxTitle: 'Image Box',
      boxPath: '../../assets/angularjs.png'
    },
    {
      boxTitle: 'Image Box',
      boxPath: '../../assets/angularjs.png'
    }
  ];

  constructor() { }

  ngOnInit() {
  }

}
