import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-img-box',
  templateUrl: './img-box.component.html',
  styleUrls: ['./img-box.component.css']
})
export class ImgBoxComponent implements OnInit {
  @Input() title;
  @Input() path;

  constructor() {
  }

  ngOnInit() {
  }
}


