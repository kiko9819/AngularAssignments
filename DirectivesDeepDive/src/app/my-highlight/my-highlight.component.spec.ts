import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyHighlightComponent } from './my-highlight.component';

describe('MyHighlightComponent', () => {
  let component: MyHighlightComponent;
  let fixture: ComponentFixture<MyHighlightComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyHighlightComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyHighlightComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
