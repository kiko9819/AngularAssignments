import {
  Directive, ElementRef,
  HostBinding,
  HostListener,
  Input,
  OnInit, Renderer2
} from "@angular/core";

enum Colors {
  red = '#f8d7da',
  blue = '#cce5ff',
  green = '#d4edda',
  yellow = '#fff3cd',
  default = 'transparent'
}

@Directive({
  selector: '[myHighlight]'
})
export class MyHighlightDirective implements OnInit {
  @Input() chosenColor: string;
  @HostBinding('style.backgroundColor') backgroundColor: string = 'transparent';
  rendererClass;

  constructor(private htmlElement: ElementRef, private renderer: Renderer2) {
  }

  ngOnInit() {
    this.renderer.setStyle(this.htmlElement.nativeElement, 'padding', '.7rem');
  }

  @HostListener('mouseenter') mouseenter() {
    switch (this.chosenColor) {
      case 'red':
        this.setColors(Colors.red, 'danger-paragraph');
        break;
      case 'blue':
        this.setColors(Colors.blue, 'primary-paragraph');
        break;
      case 'green':
        this.setColors(Colors.green, 'success-paragraph');
        break;
      case 'yellow':
        this.setColors(Colors.yellow, 'warning-paragraph');
        break;
      default:
        this.setColors(Colors.default);
        break;
    }
    this.renderer.addClass(this.htmlElement.nativeElement, this.rendererClass);
  }

  @HostListener('mouseleave') mouseleave() {
    this.backgroundColor = 'transparent';
    this.renderer.removeClass(this.htmlElement.nativeElement, this.rendererClass);
  }

  setColors(bgColor: string, rendererClass?: string) {
    if (rendererClass) {
      this.rendererClass = rendererClass;
      this.backgroundColor = bgColor;
    }
    this.backgroundColor = bgColor;
  }
}
