import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConditionalHiddenComponent } from './conditional-hidden.component';

describe('ConditionalHiddenComponent', () => {
  let component: ConditionalHiddenComponent;
  let fixture: ComponentFixture<ConditionalHiddenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConditionalHiddenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConditionalHiddenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
