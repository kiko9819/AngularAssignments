import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-conditional-hidden',
  templateUrl: './conditional-hidden.component.html',
  styleUrls: ['./conditional-hidden.component.css']
})
export class ConditionalHiddenComponent implements OnInit {
  toggled = false;

  constructor() {
  }

  ngOnInit() {
  }

  toggle() {
    this.toggled = !this.toggled;
  }
}
