import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {MyConditionalHidden} from "./conditional-hidden/my-conditional-hidden.directive";
import {ConditionalHiddenComponent} from "./conditional-hidden/conditional-hidden.component";
import {MyHiddenComponent} from './my-hidden/my-hidden.component';
import {MyHiddenDirective} from "./my-hidden/my-hidden.directive";
import {MyUnderlineComponent} from './my-underline/my-underline.component';
import {MyUnderlineDirective} from "./my-underline/my-underline.directive";
import {MyHighlightComponent} from './my-highlight/my-highlight.component';
import {MyHighlightDirective} from "./my-highlight/my-highlight.directive";
import {FormsModule} from "@angular/forms";
import { MyShadowComponent } from './my-shadow/my-shadow.component';
import {MyShadowDirective} from "./my-shadow/my-shadow.directive";

@NgModule({
  declarations: [
    AppComponent,
    MyConditionalHidden,
    ConditionalHiddenComponent,
    MyHiddenDirective,
    MyHiddenComponent,
    MyUnderlineComponent,
    MyUnderlineDirective,
    MyHighlightComponent,
    MyHighlightDirective,
    MyShadowComponent,
    MyShadowDirective
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
