import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-my-shadow',
  templateUrl: './my-shadow.component.html',
  styleUrls: ['./my-shadow.component.css']
})
export class MyShadowComponent implements OnInit {
  xAxis;
  yAxis;
  color;
  shadowBlur;
  constructor() { }

  ngOnInit() {
  }

}
