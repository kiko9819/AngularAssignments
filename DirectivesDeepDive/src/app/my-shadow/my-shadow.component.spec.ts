import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyShadowComponent } from './my-shadow.component';

describe('MyShadowComponent', () => {
  let component: MyShadowComponent;
  let fixture: ComponentFixture<MyShadowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyShadowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyShadowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
