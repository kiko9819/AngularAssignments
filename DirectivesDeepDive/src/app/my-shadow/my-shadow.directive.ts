import {Directive, HostBinding, Input, OnInit, OnChanges, SimpleChanges} from "@angular/core";

@Directive({
  selector: '[myShadow]'
})
export class MyShadowDirective implements OnInit, OnChanges {
  @Input() myShadowBlur;
  @Input() shadowX;
  @Input() shadowY;
  @Input() myColor;
  @HostBinding('style.boxShadow') boxShadow;

  constructor() {
  }

  ngOnInit() {}

  ngOnChanges() {
    let x = `${!this.shadowX ? '1' : this.shadowX}px `;
    let y = `${!this.shadowY ? '1' : this.shadowY}px `;
    let blur = `${!this.myShadowBlur ? '1' : this.myShadowBlur}px `;
    let color = `${!this.myColor ? '#000' : this.myColor}`;

    this.boxShadow = x + y + blur + color;
  }

}
