import {Directive, ElementRef, OnInit, Renderer2} from "@angular/core";

@Directive({
  selector: '[myUnderline]'
})
export class MyUnderlineDirective implements OnInit{
  constructor(private htmlElement: ElementRef, private renderer: Renderer2){}

  ngOnInit() {
    this.renderer.setStyle(this.htmlElement.nativeElement, 'text-decoration','underline');
  }
}
