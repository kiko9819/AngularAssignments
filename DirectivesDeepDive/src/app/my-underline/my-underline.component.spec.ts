import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyUnderlineComponent } from './my-underline.component';

describe('MyUnderlineComponent', () => {
  let component: MyUnderlineComponent;
  let fixture: ComponentFixture<MyUnderlineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyUnderlineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyUnderlineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
