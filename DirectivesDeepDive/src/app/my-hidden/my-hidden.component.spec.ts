import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyHiddenComponent } from './my-hidden.component';

describe('MyHiddenComponent', () => {
  let component: MyHiddenComponent;
  let fixture: ComponentFixture<MyHiddenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyHiddenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyHiddenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
