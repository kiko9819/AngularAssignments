import {Directive, ElementRef, OnInit, Renderer2} from "@angular/core";

@Directive({
  selector: '[myHidden]'
})
export class MyHiddenDirective implements OnInit{
  constructor(private htmlElement: ElementRef, private renderer: Renderer2){}

  ngOnInit(){
      this.renderer.setStyle(this.htmlElement.nativeElement,'display','none');
  }
}
