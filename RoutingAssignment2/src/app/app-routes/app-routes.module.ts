import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {BoardGamesListComponent} from '../board-games-list/board-games-list.component';
import {NewBoardGameFormComponent} from '../new-board-game-form/new-board-game-form.component';
import {NotFoundComponent} from '../not-found/not-found.component';
import {HomeComponent} from '../home/home.component';
import {BoardGameComponent} from '../board-games-list/board-game/board-game.component';
import {AuthGuard} from '../auth-guard.service';
import {BoardGameStartComponent} from '../board-games-list/board-game-start/board-game-start.component';

const appRoutes = [
  {path: '', redirectTo: '/home', pathMatch: 'full'},
  {
    path: 'home', component: HomeComponent
  },
  {
    path: 'games', component: BoardGamesListComponent,
    children: [
      {path: '', component: BoardGameStartComponent},
      {path: ':id', canActivate: [AuthGuard], component: BoardGameComponent}
    ]
  },
  {path: 'new', canActivate: [AuthGuard], component: NewBoardGameFormComponent},
  {path: 'not-found', component: NotFoundComponent},
  {path: '**', redirectTo: '/not-found'}
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [RouterModule]
})
export class AppRoutesModule {
}
