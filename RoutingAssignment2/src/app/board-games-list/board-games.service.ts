import {Injectable} from '@angular/core';
import {BoardGame} from './board-game.model';

@Injectable({
  providedIn: 'root'
})
export class BoardGamesService {
  games: BoardGame[] = [
    new BoardGame(
      0,
      'Chess',
      12.99,
      2,
      '../../assets/chess.jpg',
      2,
      'Chess is a two-player strategy board game played on a chessboard, a checkered gameboard with 64\n' +
      'squares arranged in an 8×8 grid. The game is played by millions of people worldwide. Chess is believed to\n' +
      'have originated in India sometime before the 7th century. The game was derived from the Indian game\n' +
      'chaturanga, which is also the likely ancestor of the Eastern strategy games xiangqi, janggi, and shogi. Chess\n' +
      'reached Europe by the 9th century, due to the Moorish conquest of the Iberian Peninsula. The pieces assumed\n' +
      'their current powers in Spain in the late 15th century; the rules were standardized in the 19th century.'
    )
  ];

  constructor() {
  }

  createGame(newGame: BoardGame): void {
    this.games.push(newGame);
  }

  getGame(id: number) {
    return this.games[id];
  }
}
