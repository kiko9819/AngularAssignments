import {Component, OnInit} from '@angular/core';
import {BoardGamesService} from './board-games.service';
import {BoardGame} from './board-game.model';

@Component({
  selector: 'app-board-games-list',
  templateUrl: './board-games-list.component.html',
  styleUrls: ['./board-games-list.component.css']
})
export class BoardGamesListComponent implements OnInit {
  games: BoardGame[] = [];

  constructor(private gamesService: BoardGamesService) {
  }

  ngOnInit() {
    this.games = this.gamesService.games;
  }

}
