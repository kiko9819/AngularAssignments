import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Params} from '@angular/router';
import {BoardGame} from '../board-game.model';
import {BoardGamesService} from '../board-games.service';

@Component({
  selector: 'app-board-game',
  templateUrl: './board-game.component.html',
  styleUrls: ['./board-game.component.css']
})
export class BoardGameComponent implements OnInit {
  game: BoardGame;
  id: number;

  constructor(private route: ActivatedRoute, private gamesService: BoardGamesService) {
  }

  ngOnInit() {
    this.route.params
      .subscribe(
        (params: Params) => {
          this.id = +params['id'];
          this.game = this.gamesService.getGame(this.id);
          console.log(this.game);
        }
      );
  }
}
