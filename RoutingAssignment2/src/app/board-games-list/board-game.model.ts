export class BoardGame {
  public id: number;
  public name: string;
  public price: number;
  public itemsAvailable: number;
  public imgPath: string;
  public playersAllowedToPlay: number;
  public description: string;

  constructor(id: number, name: string, price: number, itemsAvailable: number, imgPath: string, players: number, desc: string) {
    this.id = id;
    this.name = name;
    this.price = price;
    this.itemsAvailable = itemsAvailable;
    this.imgPath = imgPath;
    this.playersAllowedToPlay = players;
    this.description = desc;
  }
}
