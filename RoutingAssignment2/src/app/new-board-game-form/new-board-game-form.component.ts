import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

import {ValidateUrl} from './url.validator';
import {BoardGamesService} from '../board-games-list/board-games.service';
import {BoardGame} from '../board-games-list/board-game.model';
import {Router} from '@angular/router';

@Component({
  selector: 'app-new-board-game-form',
  templateUrl: './new-board-game-form.component.html',
  styleUrls: ['./new-board-game-form.component.css']
})
export class NewBoardGameFormComponent implements OnInit {
  boardsForm: FormGroup;
  idCounter = 0;

  constructor(private formBuilder: FormBuilder, private boardsService: BoardGamesService, private router: Router) {
  }

  ngOnInit() {
    this.boardsForm = this.formBuilder.group({
      name: ['', Validators.required],
      price: ['', Validators.required],
      imgURL: ['', [Validators.required, ValidateUrl]],
      itemsAvailable: ['', Validators.required],
      players: ['', Validators.required],
      description: ['', Validators.required]
    });
  }

  submitData(event) {
    event.preventDefault();
    if (this.boardsForm.invalid) {
      return;
    }
    this.addNewGame();
    this.router.navigate(['/games', this.idCounter]);
  }

  addNewGame(): void {
    this.boardsService.createGame(
      new BoardGame(
        this.idCounter++,
        this.boardsForm.controls['name'].value,
        this.boardsForm.controls['price'].value,
        this.boardsForm.controls['itemsAvailable'].value,
        this.boardsForm.controls['imgURL'].value,
        this.boardsForm.controls['players'].value,
        this.boardsForm.controls['description'].value
      )
    );
  }
}
