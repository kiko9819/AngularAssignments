import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewBoardGameFormComponent } from './new-board-game-form.component';

describe('NewBoardGameFormComponent', () => {
  let component: NewBoardGameFormComponent;
  let fixture: ComponentFixture<NewBoardGameFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewBoardGameFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewBoardGameFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
