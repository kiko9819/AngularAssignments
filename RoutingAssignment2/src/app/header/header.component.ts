import {Component, OnInit} from '@angular/core';
import {AuthService} from '../auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  isLoggedIn = false;

  constructor(private authService: AuthService, private router: Router) {
  }

  ngOnInit() {
  }

  onLogin() {
    this.authService.login();
    this.isLoggedIn = this.authService.loggedIn;
    setTimeout(() => {
      this.router.navigate(['/games'] );
    }, 300);
  }

  onLogout() {
    this.authService.logout();
    this.isLoggedIn = this.authService.loggedIn;
    setTimeout(() => {
      this.router.navigate(['/']);
    }, 300);
  }

}
