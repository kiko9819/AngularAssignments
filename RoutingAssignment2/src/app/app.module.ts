import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';

import {AppComponent} from './app.component';
import {HeaderComponent} from './header/header.component';
import {BoardGamesListComponent} from './board-games-list/board-games-list.component';
import {NewBoardGameFormComponent} from './new-board-game-form/new-board-game-form.component';
import {NotFoundComponent} from './not-found/not-found.component';
import {AppRoutesModule} from './app-routes/app-routes.module';
import {HomeComponent} from './home/home.component';
import {BoardGameComponent} from './board-games-list/board-game/board-game.component';
import {AuthService} from './auth.service';
import {AuthGuard} from './auth-guard.service';
import {BoardGameStartComponent} from './board-games-list/board-game-start/board-game-start.component';
import {AlertModule} from 'ngx-bootstrap';


@NgModule({
  imports: [
    BrowserModule,
    AppRoutesModule,
    ReactiveFormsModule,
  ],
  declarations: [
    AppComponent,
    HeaderComponent,
    BoardGamesListComponent,
    NewBoardGameFormComponent,
    NotFoundComponent,
    HomeComponent,
    BoardGameComponent,
    BoardGameStartComponent
  ],
  entryComponents: [
    BoardGameComponent
  ],
  providers: [AuthService, AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule {
}
