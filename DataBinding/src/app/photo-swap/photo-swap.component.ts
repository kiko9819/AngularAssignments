import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-photo-swap',
  templateUrl: './photo-swap.component.html',
  styleUrls: ['./photo-swap.component.css']
})
export class PhotoSwapComponent implements OnInit {
  imgUrl = '../assets/pic2.jpeg';
  imgUrlSwap = '../assets/pic1.jpeg';
  tempUrl;

  constructor() { }

  ngOnInit() {
  }

  swapUrls() {
    this.tempUrl = this.imgUrl;
    this.imgUrl = this.imgUrlSwap;
    this.imgUrlSwap = this.tempUrl;
  }

}
