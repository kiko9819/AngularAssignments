import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-person',
  templateUrl: './person.component.html',
  styleUrls: ['./person.component.css']
})
export class PersonComponent implements OnInit {
  person = {
    name: 'Kristiyan.',
    age: 20,
    hobbies: 'Play the guitar.',
    favAnimal: 'All of them, except spiders.',
    favColor: 'white, because it contains all colors.',
    favFood: 'Pastaaa!!!'
  };

  constructor() { }

  ngOnInit() {
  }

}
